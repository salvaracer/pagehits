<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "pagehits".
 *
 * Auto generated 19-04-2016 21:17
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
  'title' => 'Pagehits',
  'description' => 'The pagehits extension extends the pages table with a new column which contains the number of visitors\' hits on this page.',
  'category' => 'plugin',
  'version' => '1.2.0',
  'state' => 'stable',
  'uploadfolder' => 0,
  'createDirs' => '',
  'clearcacheonload' => 0,
  'author' => 'Salvatore Eckel, Armin Ruediger Vieweg',
  'author_email' => 'salvatore@infoeckel.de, info@professorweb.de',
  'author_company' => 'Infoeckel, Professor Web - Webdesign Blog',
  'constraints' => array(
    'depends' => array(
      'typo3' => '6.2.0-8.99.99',
    ),
    'conflicts' => array(
    ),
    'suggests' => array(
    ),
  ),
);

