<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Pagehits');

$tempColumns = Array (
	'tx_pagehits_hits' => Array (
		'exclude' => 1,
		'label' => 'Pagehits',
		'config'  => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	)
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumns, 1);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages', '--div--;Pagehits;;;;1-1-1,tx_pagehits_hits');
